# -*- coding: utf-8 -*-

import codecs

#class NGram(GenericItem):
#	fields = [('orth', 'string'), ('count', 'integer'), ('frequency', 'float')]
#
#	def __init__(self, orth):
#		values = { 'orth' : orth, 'count' : 0, 'frequency' : 0 }
#		GenericItem.__init__(self, orth, NGram.fields, values)
#		self.orth = orth
#		self.count = 0
#		self.frequency = 0
#	
#	def to_string(self):
#		self['orth'] = self.orth
#		self['count'] = self.count
#		self['frequency'] = self.frequency
#		return GenericItem.to_string(self)
#	
#	@staticmethod
#	def from_string(string):
#		item = GenericItem.from_string(string, NGram.fields)
#		ngram = NGram(item['orth'])
#		ngram.count = item['count']
#		ngram.frequency = item['frequency']
#		return ngram
#
#class NGramContainer(GenericItemContainer):
#	def __init__(self):
#		GenericItemContainer.__init__(self, NGram.fields)
#
#	def add(self, ngram):
#		try:
#			self.items[ngram.key].count += 1
#		except KeyError:
#			self.items[ngram.key] = ngram
#			self.items[ngram.key].count += 1
#		self.count += 1
#	
#	@staticmethod
#	def load_from_file(filename):
#		ngrams_c = ItemsContainer(fields)
#		with codecs.open(filename, 'r', 'utf-8') as fp:
#			for line in fp:
#				ngrams_c.add(NGram.from_string(line.rstrip())
#		return ngrams_c

class NGram:
	def __init__(self, orth):
		self.orth = orth
		self.count = 0
		self.frequency = 0
	
	def to_string(self):
		return u'\t'.join([self.orth, str(self.count), str(self.frequency)])
	
	@staticmethod
	def from_string(string):
		data = string.split(u'\t')
		n_gram = NGram(data[0])
		n_gram.count = int(data[1])
		n_gram.frequency = float(data[2])
		return n_gram
	
class NGramsContainer:
	def __init__(self):
		self.n_grams = {}
		self.count = 0

	def __getitem__(self, key):
		return self.n_grams[key]
	
	def keys(self):
		return self.n_grams.keys()

	def add(self, key):
		try:
			self.n_grams[key].count += 1
		except KeyError:
			self.n_grams[key] = NGram(key)
			self.n_grams[key].count += 1
		self.count += 1

	def calculate_frequencies(self):
		for n_gram in self.n_grams.values():
			n_gram.frequency = float(n_gram.count) / self.count

	def save_to_file(self, filename, sortkey = lambda x: x.frequency):
		# create and sort n-grams list
		n_grams_list = sorted(self.n_grams.values(), reverse = True, key = sortkey)
		# write list to file
		with codecs.open(filename, 'w+', 'utf-8') as fp:
			for n_gram in n_grams_list:
				fp.write(n_gram.to_string() + '\n')
	
	@staticmethod
	def load_from_file(filename):
		ngrams_c = NGramsContainer()
		with codecs.open(filename, 'r', 'utf-8') as fp:
			for line in fp:
				n_gram = NGram.from_string(line.rstrip())
				key = n_gram.orth
				ngrams_c.n_grams[key] = n_gram
				ngrams_c.count += n_gram.count
		return ngrams_c
