import codecs

MAX_WORDS = 100000

def load(filename):
	wordlist = []
	count = 0
	with codecs.open(filename, 'r', 'utf-8') as fp:
		for line in fp:
			wordlist.append(line.strip())
			count += 1
			if count >= MAX_WORDS:
				break
	return wordlist
