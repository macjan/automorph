# -*- coding: utf-8 -*-

import codecs
from ngrams import *

class Ending(NGram):
	def __init__(self, orth, score = 0.0):
		NGram.__init__(self, orth)
		self.score = score
	
	def to_string(self):
		return u'\t'.join([self.orth, str(self.count), str(self.frequency), str(self.score)])
	
	@staticmethod
	def from_string(string):
		data = string.split('\t')
		ending = Ending(data[0])
		ending.count = int(data[1])
		ending.frequency = float(data[2])
		ending.score = float(data[3])
		return ending
	
class EndingsContainer(NGramsContainer):
	def __init__(self):
		NGramsContainer.__init__(self)

	def add(self, key, count = 1, frequency = 0.0, score = 0.0):
		try:
			self.n_grams[key].count += count
		except KeyError:
			self.n_grams[key] = Ending(key, score)
			self.n_grams[key].count = count
			self.n_grams[key].frequency = frequency
		self.count += count
	
	@staticmethod
	def load_from_file(filename, filter_fun = lambda x: True):
		endings_c = EndingsContainer()
		with codecs.open(filename, 'r', 'utf-8') as fp:
			for line in fp:
				ending = Ending.from_string(line.rstrip())
				if not filter_fun(ending):
					continue
				key = ending.orth
				endings_c.n_grams[key] = ending
				endings_c.count += ending.count
		return endings_c
