import datastruct.endings
import datastruct.endingsets
import datastruct.stems
import datastruct.wordlist
import settings

def run(wordlist_file):
	print 'Loading word list...'
	wordlist = datastruct.wordlist.load(wordlist_file)
	print 'Loading endings list...'
	endings_c = datastruct.endings.EndingsContainer.load_from_file('endings.txt', \
		settings.ENDINGS_FILTER)
	print 'Creating trie...'
	trie = datastruct.stems.StemTrie(endings_c)
	count = 0
	for word in wordlist:
		trie.add(word)
		count += 1
		if count % 100000 == 0:
			print count
	print 'Extracting ending sets...'
	endingsets_c = datastruct.endingsets.EndingSetsContainer()
	for stem, endings in trie.list_stems():
		#print stem, datastruct.endingsets.EndingSet.endings_to_string(endings)
		endingsets_c.add(datastruct.endingsets.EndingSet.endings_to_string(endings))
	del wordlist
	print 'Saving trie...'
	trie.save_to_file('trie.txt')
	del trie
	print 'Saving ending sets...'
	endingsets_c.save_to_file('endingsets.txt', sortkey = lambda x: x.count)
