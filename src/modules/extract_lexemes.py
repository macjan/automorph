import datastruct.endings
import datastruct.endingsets
import datastruct.stems
import settings
import codecs

def run(wordlist_filename):
	print 'Loading endings...'
	endings_c = datastruct.endings.EndingsContainer.load_from_file('endings.txt',\
		filter_fun = settings.ENDINGS_FILTER)
	print 'Loading word list...'
	wordlist = datastruct.wordlist.load(wordlist_filename)
	print 'Loading trie...'
	trie = datastruct.stems.StemTrie.load_from_file('trie_stems_split.txt', endings_c)
	print 'Extracting lexemes...'
	lexemes = {}
	for word in wordlist:
		lex = set([word])
		for i in range(1, len(word)+1):
			stem, ending = None, None
			try:
				stem, ending = word[:i], endings_c[word[i:]]
			except KeyError:
				continue
			for es in trie[word[:i]].endingsets:
				if ending in es:
					for another_ending in es:
						lex.add(stem + another_ending.orth)
		lex_list = sorted(list(lex))
		lexemes[lex_list[0]] = lex_list
	print 'Writing lexemes...'
	with codecs.open('lexemes.txt', 'w+', 'utf-8') as fp:
		for _, words in lexemes.iteritems():
			fp.write(u', '.join(words) + '\n')
