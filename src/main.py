# Automorph (c) Maciej Janicki 2012
# See README file for usage notes.
# Caution: This code is experimental and is NOT intended as a ready-to-use application.
#          It is provided without any warranty.

def main(args):
	import modules.score_endings
	import modules.create_stem_trie
	import modules.split_ending_sets
	import modules.extract_lexemes
	import modules.evaluate
	modules.score_endings.run(args[1])
	modules.create_stem_trie.run(args[1])
	modules.split_ending_sets.run()
	modules.extract_lexemes.run(args[1])
	modules.evaluate.run(args[1], args[2])

if __name__ == '__main__':
	import sys
	if len(sys.argv) < 2 or sys.argv[1] == '--help':
		print 'See README file for usage notes.'
	else:
		main(sys.argv)
